# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class CColonias(models.Model):
    id_asenta_cpcons = models.CharField(primary_key=True, max_length=4)
    d_codigo = models.CharField(max_length=5, blank=True, null=True)
    d_asenta = models.CharField(max_length=60, blank=True, null=True)
    d_tipo_asenta = models.CharField(max_length=40, blank=True, null=True)
    d_ciudad = models.CharField(max_length=50, blank=True, null=True)
    d_cp = models.CharField(max_length=5, blank=True, null=True)
    c_oficina = models.CharField(max_length=5, blank=True, null=True)
    c_cp = models.CharField(db_column='c_CP', max_length=5, blank=True, null=True)  # Field name made lowercase.
    c_tipo_asenta = models.CharField(max_length=2, blank=True, null=True)
    d_zona = models.CharField(max_length=40, blank=True, null=True)
    c_cve_ciudad = models.CharField(max_length=2, blank=True, null=True)
    c_estado = models.ForeignKey('CEstados', models.DO_NOTHING, db_column='c_estado', blank=True, null=True)
    c_mnpio = models.ForeignKey('CMunicipios', models.DO_NOTHING, db_column='c_mnpio', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'c_colonias'


class CEstados(models.Model):
    c_estado = models.CharField(primary_key=True, max_length=2)
    d_estado = models.CharField(max_length=35, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'c_estados'


class CMunicipios(models.Model):
    c_mnpio = models.CharField(primary_key=True, max_length=3)
    d_mnpio = models.CharField(max_length=50, blank=True, null=True)
    c_estado = models.ForeignKey(CEstados, models.DO_NOTHING, db_column='c_estado', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'c_municipios'
