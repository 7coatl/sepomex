from rest_framework import serializers
from .models import CColonias


class CColoniasSerializer(serializers.ModelSerializer):
    #text = serializers.CharField(max_length=1000, required=True)

    class Meta:
        model = CColonias 
        fields = (
            'id_asenta_cpcons',
            'd_codigo',
            'd_asenta',
            'd_tipo_asenta',
            'd_ciudad',
            'd_cp',
            'c_oficina',
            'c_cp',
            'c_tipo_asenta',
            'd_zona',
            'c_cve_ciudad',
            'c_estado',
            'c_mnpio'
    )