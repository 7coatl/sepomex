from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets

from .models import CColonias
from .serializers import CColoniasSerializer

class Cosa(APIView):
    def get(self, request):
        content = {'message': 'Hello! This is a Demo!'}
        return Response(content)

class DemoView(APIView):
    def get(self, request, id=None):
        if id:
            try:
                q = CColonias.objects.get(id=id)
            except CColonias.DoesNotExist:
                return Response({'errors':'No existe la colonia.'}, status=400)
            rs = CColoniasSerializer(q)
        else:
           q = CColonias.objects.all()
           rs = CColoniasSerializer(q, many=True)

        return Response(rs.data)