DROP TABLE IF EXISTS c_estados;
CREATE TABLE c_estados (
	c_estado varchar(2) primary key,
	d_estado varchar(35)
);

DROP TABLE IF EXISTS c_minicipios;
CREATE TABLE c_municipios (
	c_mnpio varchar(3) primary key,
	d_mnpio varchar(50),
	c_estado varchar(2),
	FOREIGN KEY(c_estado) REFERENCES c_estados(c_estado)
);


DROP TABLE IF EXISTS c_colonias;
CREATE TABLE c_colonias (
	id_asenta_cpcons varchar(4) primary key,
	d_codigo varchar(5),
	d_asenta varchar(60),
	d_tipo_asenta varchar(40),
	d_ciudad varchar(50),
	d_cp varchar(5),
	c_oficina varchar(5),
	c_CP varchar(5),
	c_tipo_asenta varchar(2),
	d_zona varchar(40),
	c_cve_ciudad varchar(2),
	c_estado varchar(2),
	c_mnpio varchar(3),
	FOREIGN KEY(c_mnpio) REFERENCES c_municipios(c_mnpio),
	FOREIGN KEY(c_estado) REFERENCES c_estados(c_estado)
);

