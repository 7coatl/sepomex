import os
import bonobo
from bonobo.config import use_raw_input
import MySQLdb as mdb


def extract(encoding):
    """Extraemos los datos del archivo de sepomex"""
    #return bonobo.CsvReader(path='CPdescarga.txt', delimiter='|')
    return bonobo.CsvReader(path='CPdescarga.txt', delimiter='|', encoding=encoding or 'utf-8'),



#def transform(*args):
#    yield tuple(
#        map(str.title, args)
#    )


def load(*args):   
    try:   
        con = mdb.connect('localhost', 'root', 'adminadmin', 'sepomex');
        #mdb.autocommit(True)
        
        cur = con.cursor()
        
        #Insertamos los estados
        q= "SELECT * FROM c_estados WHERE c_estado='%s'" % (args[7]);
        cur.execute(q)

        if cur.fetchone() == None:
            q = "INSERT INTO c_estados (c_estado, d_estado) VALUES ('%s', '%s')" % (args[7], args[4])
            print(q)
            cur.execute(q)
    
        #Insertamos los municipios
        q= "SELECT * FROM c_municipios WHERE c_mnpio='%s'" % (args[11]);
        cur.execute(q)
        
        if cur.fetchone() == None:
            q = "INSERT INTO c_municipios (c_mnpio, d_mnpio, c_estado) VALUES ('%s', '%s', '%s')" % (args[11], args[3], args[7])
            print(q)
            cur.execute(q)
            
        #Insertamos las colonias
        q= "SELECT * FROM c_colonias WHERE id_asenta_cpcons='%s'" % (args[12]);
        cur.execute(q)

        if cur.fetchone() == None:
            q = "INSERT INTO c_colonias (id_asenta_cpcons, d_codigo, d_asenta, d_tipo_asenta, d_ciudad, d_cp, c_oficina, c_CP, c_tipo_asenta, d_zona, c_cve_ciudad, c_estado, c_mnpio) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (args[12], args[0], args[1], args[2], args[5], args[6], args[8], args[9], args[10], args[13], args[14], args[7], args[11])
            print(q)
            cur.execute(q)
            
        con.commit()
            
    except mdb.Error:
        print("Error %d: %s" % (e.args[0],e.args[1]))
    finally:        
        if con:    
            con.close()
            
def get_graph(*, encoding=None):
    """
    This function builds the graph that needs to be executed.

    :return: bonobo.Graph

    """
    graph = bonobo.Graph()
    graph.add_chain(
	bonobo.CsvReader(path='CPdescarga.txt', delimiter='|', encoding=encoding or 'utf-8'),
    	#extract, 
    	#transform, 
    	load
    )

    return graph


def get_services(**options):
    """
    This function builds the services dictionary, which is a simple dict of names-to-implementation used by bonobo
    for runtime injection.

    It will be used on top of the defaults provided by bonobo (fs, http, ...). You can override those defaults, or just
    let the framework define them. You can also define your own services and naming is up to you.

    :return: dict
    """
    return {}


# The __main__ block actually execute the graph.
if __name__ == '__main__':
    parser = bonobo.get_argument_parser()
    with bonobo.parse_args(parser) as options:
        bonobo.run(
            get_graph(encoding=os.getenv('encoding')),
            #get_graph(**options),
            services=get_services(**options)
        )
